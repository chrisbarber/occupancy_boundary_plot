import argparse
import logging
import os
import subprocess

import pandas as pd
import numpy as np
from numpy.lib.format import open_memmap


def average_occupancy(
        normalize_length,
        boundary_length,
        gff_file_path,
        gff_chunk_size,
        occupancy_chunk_size,
        occupancy_data_dir,
        recache,
        annotation_label,
        ):

    logging.debug('getting unique chromosomes in {}'.format(gff_file_path))
    chromosome_keys = set()
    gff_reader = pd.read_table(
        gff_file_path,
        chunksize=gff_chunk_size,
        header=None,
        usecols=[0],
        memory_map=True,
        )
    for chunk in gff_reader:
        chromosome_keys = chromosome_keys.union(chunk[0].unique())

    # create memmaps of occupancy files (or open if cached and recache=False)
    occupancy = {}
    for chromosome_key in chromosome_keys:
        occupancy_file_path = os.path.join(occupancy_data_dir, '{}.occupancy'.format(chromosome_key))
        npy_file_path = '{}.npy'.format(occupancy_file_path)
        if not os.path.isfile(npy_file_path) or recache:
            logging.debug('opening {} and converting to memmap {}'.format(occupancy_file_path, npy_file_path))
            wc_output = subprocess.run(['wc', '-l', occupancy_file_path], stdout=subprocess.PIPE)
            line_count = int(str(wc_output.stdout).split()[1])
            mmap = open_memmap(npy_file_path, mode='w+', dtype=np.double, shape=(line_count,))

            # read occupancies
            n = 0
            for chunk in pd.read_csv(occupancy_file_path, chunksize=occupancy_chunk_size, header=None):
                mmap[n:n+chunk.shape[0]] = chunk.values.flat
                n += chunk.shape[0]
        else:
            mmap = open_memmap(npy_file_path, mode='r')
        occupancy[chromosome_key] = mmap

    # read, normalize, accumulate average
    region_average = np.zeros((normalize_length,))
    before_average = np.zeros((boundary_length,))
    after_average = np.zeros((boundary_length,))
    num_regions = 0
    gff_reader = pd.read_table(
        gff_file_path,
        chunksize=gff_chunk_size,
        header=None,
        usecols=[0,2,3,4],
        memory_map=True,
        )
    for chunk in gff_reader:
        chunk = chunk[chunk[2] == annotation_label]
        for annotation in chunk.iterrows():
            num_regions += 1
            logging.debug(num_regions)
            chromosome, annot, start_pos, end_pos = annotation[1]

            if start_pos == end_pos:
                continue

            region = occupancy[chromosome][start_pos:end_pos]
            normalized_region = np.interp(np.arange(0.,region.size,region.size/normalize_length),np.arange(0,region.size),region)
            normalized_region = normalized_region[:normalize_length] #TODO: shouldn't be needed but sometimes interp ends up with an extra element
            before = occupancy[chromosome][max(0,start_pos-boundary_length):start_pos]
            after = occupancy[chromosome][end_pos:min(occupancy[chromosome].size,end_pos+boundary_length)]

            region_average = region_average-region_average/num_regions+normalized_region/num_regions
            before_average[-before.size:] -= before_average[-before.size:]/num_regions+before/num_regions
            after_average[:after.size] -= after_average[:after.size]/num_regions+after/num_regions

    return np.concatenate((before_average, region_average, after_average))


if __name__ == '__main__':

    logging.getLogger('').setLevel(logging.INFO)

    parser = argparse.ArgumentParser(
        description='',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )

    parser.add_argument('--normalize-length', default=25, help='Integer length to normalize regions to')
    parser.add_argument('--boundary-length', default=100, help='Fixed length for boundaries to include in average')
    parser.add_argument('--gff-file-path', help='Path to GFF input file')
    parser.add_argument('--gff-chunk-size', default=1000, help='Size of chunks to read from GFF file')
    parser.add_argument('--occupancy-data-dir', help='Path to directory containing *.occupancy files for each chromosome in GFF')
    parser.add_argument('--occupancy-chunk-size', default=10000, help='Size of chunks to read from occupancy files')
    parser.add_argument('--recache', type=bool, default=False, help='Force recache of temporary *.npy data copied from occupancy files')
    parser.add_argument('--annotation-label', type=str, default='exon', help='Annotation label to look for in GFF')

    args = parser.parse_args()

    plot = average_occupancy(
        args.normalize_length,
        args.boundary_length,
        args.gff_file_path,
        args.gff_chunk_size,
        args.occupancy_chunk_size,
        args.occupancy_data_dir,
        args.recache,
        args.annotation_label,
        )

    print(plot.tolist())
